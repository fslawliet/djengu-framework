<a id="readme-top"></a>

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=for-the-badge)](https://github.com/prettier/prettier)
![GitLab (self-managed) contributors](https://img.shields.io/gitlab/contributors/fslawliet%2Fdjengu-framework?style=for-the-badge)
![Gitlab code coverage (self-managed, specific job)](https://img.shields.io/gitlab/pipeline-coverage/fslawliet%2Fdjengu-framework?style=for-the-badge)
![Gitlab pipeline status (self-managed)](https://img.shields.io/gitlab/pipeline-status/fslawliet%2Fdjengu-framework?branch=main&style=for-the-badge)
![Mastodon Follow](https://img.shields.io/mastodon/follow/000166251?domain=https%3A%2F%2Fmasto.donte.com.br&style=for-the-badge)
![GitLab all issues](https://img.shields.io/gitlab/issues/all/fslawliet%2Fdjengu-framework?style=for-the-badge)
![GitLab last commit](https://img.shields.io/gitlab/last-commit/fslawliet%2Fdjengu-framework?style=for-the-badge)
![GitLab (self-managed)](https://img.shields.io/gitlab/license/fslawliet%2Fdjengu-framework?style=for-the-badge)
![GitLab forks](https://img.shields.io/gitlab/forks/fslawliet%2Fdjengu-framework?style=for-the-badge)
![GitLab stars](https://img.shields.io/gitlab/stars/fslawliet%2Fdjengu-framework?style=for-the-badge)
![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/fslawliet%2Fdjengu-framework?style=for-the-badge)
![Lines of code](https://img.shields.io/tokei/lines/gitlab/fslawliet/djengu-framework?style=for-the-badge)

<br />

<div align="center">
<img src="frontend/src/assets/djengu-logo.svg"
     alt="logo image" width="230" />
<h1 align="center">Djengu Framework</h1>
<p align="center">
<em> Um ambiente de desenvolvimento e produção completo para Django e Vue. </em>
<br />
    <a href="https://fslawliet.gitlab.io/djengu-framework/"><strong>Veja a nossa documentação »</strong></a>
    <br />
    <br />
    <a href="#">Veja a página de exemplo</a>
    ·
    <a href="https://gitlab.com/fslawliet/djengu-framework/-/issues">Achou um bug? Conte pra nós!</a>
    ·
    <a href="https://gitlab.com/fslawliet/djengu-framework/-/issues">Teve uma ideia? Conte também!</a>
</p>
</div>
<details>
  <summary>Índice</summary>
  <ol>
    <li>
      <a href="#sobre-o-projeto">Sobre o Projeto</a>
      <ul>
        <li><a href="#feito-com">Feito com</a></li>
      </ul>
    </li>
    <li>
      <a href="#começando">Começando</a>
      <ul>
        <li><a href="#pré-requisitos">Pré-requisitos</a></li>
        <li><a href="#instalação">Instalação</a></li>
      </ul>
    </li>
    <li><a href="#como-usar">Como Usar</a></li>
    <li><a href="#roteiro-de-atualizações">Roteiro de Atualizações</a></li>
    <li><a href="#como-contribuir">Como Contribuir</a></li>
    <li><a href="#changelog">Changelog</a></li>
    <li><a href="#licença">Licença</a></li>
    <li><a href="#contato">Contato</a></li>
    <li><a href="#reconhecimentos">Reconhecimentos</a></li>
  </ol>
</details>

## Sobre o Projeto

[![Djengu Framework Screen Shot][product-screen]](#)

Djengu is a framework for creating decoupled web applications with Django and Vue.
It's essentially a full-stack cookie-cutter. Most of the the heavy lifting in
setting up both development and production environments is taken care of, such as
server set-up, mock environments, containerization, SSL/TLS, DNS, testing, and much more.

The concept behind Djengu is that it will remove all reliance on itself once set up.
Djengu will create everything you need, then quietly remove itself –
leaving you with a clean, reliable, production-ready Django/Vue application.

At the moment, Djengu is limited to Django and Quasar Framework. Support for
Vue CLI and Nuxt.js will be added soon. The tool is currently tested for Linux,
but this will extend to OSX soon.

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

### Feito com

Because Djengu takes care of a lot of underlying technologies, certain
choices have already been made. This results in more simplicity and
speed, but less flexibility. Djengu will install with the
following technologies.

- [![Apache Cordova][Cordova]][Cordova-url]
- [![Cypress][Cypress.js]][Cypress-url]
- [![Django][Django]][Django-url]
- [![Django Rest Auth][Django.Rest.Auth]][Django.Rest.Auth-url]
- [![Django Rest Framework][Django.Rest]][Django.Rest-url]
- [![Docker][Docker]][Docker-url]
- [![Gitlab CI/CD][Gitlab]][Gitlab-url]
- [![Histoire][Histoire]][Histoire-url]
- [![JWT Authentication][Jwt]][Jwt-url]
- [![Make][Makefile]][Makefile-url]
- [![PostgreSQL][PostgreSQL]][PostgreSQL-url]
- [![PWA][PWA]][PWA-url]
- [![Quasar][Quasar.js]][Quasar-url]
- [![Vagrant][Vagrant]][Vagrant-url]
- [![Vite][Vite]][Vite-url]
- [![Vitest][Vitest]][Vitest-url]
- [![Vue][Vue.js]][Vue-url]

If you'd like to see more choices, please consider contributing to the
project.

## Começando

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Pré-requisitos

Djengu requires that the following tools be installed locally before starting.

– python-virtualenv

– node.js

– npm

This is an example of how to list things you need to use the software and how to install them.

- npm
  ```sh
  npm install npm@latest -g
  ```

### Instalação

First, clone the repository, replacing `myproject` below with your desired project name.

```bash
git clone https://github.com/johnckealy/djengu.git myproject
cd myproject
```

Djengu is controlled by a `Makefile`. To start, simply run

```bash
make
```

After a quick initial set up, you can run

```bash
make backend-serve
```

then, in a new tab, run

```bash
make frontend-serve
```

and your application will be ready.

NOTE: If you chose the option that includes `authentication` during setup, you will
find that the development environment runs with SSL. You'll need to self-sign
the cert – this is as simple as "accepting the risk" when prompted by your browser.
You may also need to do this for your backend address as well (just click `VISIT DJANGO API`
on the home page).

## Como Usar

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://fslawliet.gitlab.io/djengu-framework/)_

_For more information on howo customize the Quasar configuration, see [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js)_

### Simulating the production environment

Before setting up on a real server, it can be extremely useful to simulate
the production environment. Djengu makes use of Vagrant, a tool that allows you to make
changes in your dev environment while mirroring the production code in a virtual machine.

To use this feature, you'll need to install [Vagrant](https://www.vagrantup.com/downloads)
and [VirtualBox](https://www.virtualbox.org/wiki/Downloads).

The `Vagrantfile` controls the set up. If you don't wish to use this feature, you can
simply remove it.

Create the environment with

```bash
make configure-vagrant
```

This may take a few minutes the first time it is run.

When the VM is set up, you may enter it using

```bash
vagrant ssh
```

If the setup completed successfully, there should be an instance of CaddyServer running in the VM.
You may verify this using

```bash
docker ps
```

If there are any issues at this stage, `cd` into the `/caddy` directory in the VM and check
the `Caddyfile` for any errors.

Now, `cd` into the `/djengu` directory in the VM. You should see your project. If you make
an edit in your local machine, Vagrant will mirror these changes in the VM, allowing you to
tweak your production settings without needing to make changes in two different places.

Before running the deploy, it's a good idea to check the `env/.env.prod` file to make
sure your settings are correct.

When you're happy, run

```bash
make deploy
```

This will simulate a real deployment. To see the application, simply visit the domain
you entered during the set up. If you tweak this domain, or any other settings that
may affect the CaddyServer, remember to re-run `make configure-vagrant` from your
local machine (not the VM).

NOTE: The reason you were prompted for a password when running `make configure-vagrant`
is because Djengu needed access to the `/etc/hosts` file. This is the only change
Djengu makes to your machine outside of it's own repository. When you're finished
with the Vagrant testing, it's a good idea to open `/etc/hosts` and remove these lines.
If you use the same domain for testing and deployment, the hosts entry may
interfere with your access to the real application.

To remove the VM, run

```bash
vagrant destroy
```

from the root directory.

### Vscode

If you are a Visual Studio Code user, there are some premade scripts
for running the backend application in the `.vscode/` directory. If you don't
use vscode, just go ahead and delete this directory.

### Hosting multiple web applications on the same server instance

Because Djengu is decoupled by design, it is possible to host
several completely independent applications on the same server
instance. This is done using CaddyServer's built-in reverse-proxy.

This is really useful for running many independent websites on the
same VPS instance. You could theoretically run a developer's whole
portfolio of low-traffic websites for $5 a month with this approach.

Everything is controlled from the `Caddyfile`, which will
install at `/caddy/Caddyfile` in the vagrant VM. You can add
extra applications and then reference each docker container.
The important thing to remember when doing this is to use
unique port numbers for each instance, and reference these
port numbers in the `Caddyfile`. In production, you may place the `caddy/`
directory anywhere you wish, but you must add each individual
application to the same `Caddyfile`.

### Flavours

Djengu currently has two choices. More will be added as the project progresses.

1. `Basic`: Creates a basic Quasar and Django application.

2. `Authentication`: Django and Quasar with JWT token authentication out of the box

### Test Users

If you build Djengu with authentication, there is a test
account built in. The credentials are

```
username: guest
password: secret
```

You can also log in with username `admin` and the admin password
you entered during setup.

### API

Djengu applications are based on a `Makefile`, which you'll find
in the root directory. Here's a summary of what is possible.
Feel free to add your own `Make` recipes to the file.

#### `make`

Run the setup script.

#### `make build-*`

See the make file for the various options. This runs the various
builders for the backend and frontend.

#### `make configure-vagrant`

Run this to setup the Vagrant virtual machine. The initial
run of this command will set up the virtual machine based
on the `.djengu/.production_toolbox/server_setup.sh` set
up script. You can also use this script on a real server,
e.g. when initializing a DigitalOcean droplet.

If you make changes to any domain names or any other server
related settings in the `.env.prod` file, remember to
re-run this recipe.

#### `make clean`

Remove all build files and start everything fresh.

#### `make deploy`

Deploy the application on the production server (or vagrant VM).

#### `make decrypt-dotenv`

You'll need to run this command outside of the `make` recipe,
because you must replace `foo` with the actual key. DO NOT
ADD THE DECRYPTION KEY TO THE MAKEFILE.

#### `make encrypt-dotenv`

Encrypt the `env/` folder. Running this recipe will prompt you
to create an encryption passphrase. You can add this passphrase
as a Github secret to allow Github actions to apply the CI/CD.

The recipe will create a file named `env.tar.gpg`. You may check
this file into version control, just be sure not to check in any
reference to the decryption passphrase. Use the command inside
the `make decrypt-dotenv` recipe to decypt on the server, or in CI/CD
(use the command, not the recipe itself, since the key must be set manually).

#### `make frontend-serve`

Run the development environment for the frontend.

#### `make backend-serve`

Run the development environment for the backend.

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

## Roteiro de Atualizações

- [x] Add Changelog
- [x] Add back to top links
- [ ] Add Additional Templates w/ Examples
- [ ] Add "components" document to easily copy & paste sections of the readme
- [ ] Multi-language Support
  - [ ] Chinese
  - [ ] Spanish

See the [open issues](https://github.com/othneildrew/Best-README-Template/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- CONTRIBUTING -->

## Como Contribuir

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- LICENSE -->

## Changelog

See `CHANGELOG.md` for more information.

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- LICENSE -->

## Licença

Distributed under the MIT License. See `LICENSE.md` for more information.

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- CONTACT -->

## Contato

Your Name - [@your_twitter](https://twitter.com/your_username) - email@example.com

Project Link: [https://github.com/your_username/repo_name](https://github.com/your_username/repo_name)

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- ACKNOWLEDGMENTS -->

## Reconhecimentos

Use this space to list resources you find helpful and would like to give credit to. I've included a few of my favorites to kick things off!

- [Choose an Open Source License](https://choosealicense.com)
- [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
- [Malven's Flexbox Cheatsheet](https://flexbox.malven.co/)
- [Malven's Grid Cheatsheet](https://grid.malven.co/)
- [Img Shields](https://shields.io)
- [GitHub Pages](https://pages.github.com)
- [Font Awesome](https://fontawesome.com)
- [React Icons](https://react-icons.github.io/react-icons/search)

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

[![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

Esta obra tem a [licença Creative Commons Atribuição-NãoComercial-CompartilhaIgual 4.0 Internacional][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.pt_BR
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
[product-screen]: ./screenshot.png
[Cordova]: https://img.shields.io/badge/Cordova-4CC2E4?style=for-the-badge&logo=apachecordova&logoColor=white
[Cordova-url]: https://cordova.apache.org/
[Cypress.js]: https://img.shields.io/badge/Cypress-F0FCF8?style=for-the-badge&logo=cypress&logoColor=black
[Cypress-url]: https://www.cypress.io/
[Django]: https://img.shields.io/badge/Django-0C4B33?style=for-the-badge&logo=django&logoColor=white
[Django-url]: https://www.djangoproject.com
[Django.Rest]: https://img.shields.io/badge/Django%20Rest%20Framework-A30000?style=for-the-badge&logo=django&logoColor=white
[Django.Rest-url]: https://www.django-rest-framework.org
[Django.Rest.Auth]: https://img.shields.io/badge/Django%20Rest%20Auth-2980B9?style=for-the-badge&logo=django&logoColor=white
[Django.Rest.Auth-url]: https://dj-rest-auth.readthedocs.io
[Docker]: https://img.shields.io/badge/docker-2496ED?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://www.docker.com/
[Gitlab]: https://img.shields.io/badge/gitlab%20CI/CD-FC6D26?style=for-the-badge&logo=gitlab&logoColor=white
[Gitlab-url]: https://docs.gitlab.com/ee/ci/
[Histoire]: https://img.shields.io/badge/Histoire-213547?style=for-the-badge&logo=data:image/svg%2bxml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iNTEyIgogICBoZWlnaHQ9IjUxMiIKICAgdmlld0JveD0iMCAwIDUxMiA1MTIiCiAgIHZlcnNpb249IjEuMSIKICAgaWQ9InN2ZzUiCiAgIHNvZGlwb2RpOmRvY25hbWU9ImxvZ28uc3ZnIgogICBpbmtzY2FwZTpleHBvcnQtZmlsZW5hbWU9Ii9ob21lL2Frcnl1bS9QaWN0dXJlcy9Mb2dvcy9oaXN0b2lyZS5wbmciCiAgIGlua3NjYXBlOmV4cG9ydC14ZHBpPSIxMTAuMDI0IgogICBpbmtzY2FwZTpleHBvcnQteWRwaT0iMTEwLjAyNCIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4xLjEgKDNiZjVhZTBkMjUsIDIwMjEtMDktMjApIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBpZD0ibmFtZWR2aWV3MTEiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEuMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwLjAiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBzaG93Z3JpZD0iZmFsc2UiCiAgICAgaW5rc2NhcGU6em9vbT0iMS41MTU2MjUiCiAgICAgaW5rc2NhcGU6Y3g9IjI1NS42NzAxIgogICAgIGlua3NjYXBlOmN5PSIyNTYiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIyNTYwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEzNzEiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjAiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9IjAiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmc1IiAvPgogIDxkZWZzCiAgICAgaWQ9ImRlZnMyIiAvPgogIDxnCiAgICAgaWQ9ImxheWVyMSI+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im9wYWNpdHk6MC41O2ZpbGw6IzM0ZDM5OTtmaWxsLW9wYWNpdHk6MTtzdHJva2Utd2lkdGg6MS4wMDM3NSIKICAgICAgIGlkPSJyZWN0MTM3MiIKICAgICAgIHdpZHRoPSIzMTQuMzA5MjMiCiAgICAgICBoZWlnaHQ9IjQwNi42MDkwMSIKICAgICAgIHg9Ii0yNi41NjUwNjMiCiAgICAgICB5PSIxMzQuNzUwNzkiCiAgICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtMjMuODIxMjYyKSIKICAgICAgIHJ5PSI4IiAvPgogICAgPHJlY3QKICAgICAgIHN0eWxlPSJmaWxsOiMzNGQzOTk7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlLXdpZHRoOjEuMDAzNzUiCiAgICAgICBpZD0icmVjdDg1MCIKICAgICAgIHdpZHRoPSIzMTQuMzA5MjMiCiAgICAgICBoZWlnaHQ9IjQwNi42MDkwMSIKICAgICAgIHg9Ijc3LjU3MTgzOCIKICAgICAgIHk9IjcyLjgwODcwOCIKICAgICAgIHJ5PSI4IgogICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTQuNTc0NDUzNCkiIC8+CiAgPC9nPgogIDxnCiAgICAgaWQ9ImxheWVyMyI+CiAgICA8cGF0aAogICAgICAgaWQ9InBhdGgxNjU3LTMiCiAgICAgICBzdHlsZT0iZGlzcGxheTppbmxpbmU7ZmlsbDojYjRmYWUyO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojYjRmYWUyO3N0cm9rZS13aWR0aDo4LjM0OTIzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICBkPSJNIDM1OS4zODk0NywzNTUuOTUxMzQgMzIwLjcyOTM1LDE3Ni41Mjk0MiAyMzguMzQ2MTMsMjM4Ljk0MTE4IFogTSAyNzMuNjQxMjQsMjczLjA2NjA4IDE1Mi41OTc4OCwxNTYuMDU1OTEgMTkxLjI1ODA0LDMzNS40Nzc4NiBaIiAvPgogIDwvZz4KICA8ZwogICAgIGlkPSJsYXllcjIiCiAgICAgc3R5bGU9ImRpc3BsYXk6bm9uZSI+CiAgICA8cGF0aAogICAgICAgaWQ9InBhdGgxNjU3IgogICAgICAgc3R5bGU9ImZpbGw6I2I0ZmFlMjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6I2I0ZmFlMjtzdHJva2Utd2lkdGg6ODtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgZD0ibSAyOTYsMTAzLjk4MjQyIC0xMzUuNTMxMjUsMTc3Ljk2NjggaCA4OC43MDExNyB6IE0gMjYyLjgzMDA4LDIzMC4wNTA3OCAyMTYsNDA4LjAxNzU4IDM1MS41MzEyNSwyMzAuMDUwNzggWiIKICAgICAgIHRyYW5zZm9ybT0icm90YXRlKC00LjE1NjU1MywyNTYsMjU2LjAwNjkxKSIgLz4KICA8L2c+Cjwvc3ZnPgo=&logoColor=white
[Histoire-url]: https://histoire.dev
[Jwt]: https://img.shields.io/badge/JWT%20Authentication-FFFFFF?style=for-the-badge&logo=jsonwebtokens&logoColor=black
[Jwt-url]: https://jwt.io/
[Makefile]: https://img.shields.io/badge/Make-000000?style=for-the-badge&logo=data%3Aimage%2Fsvg%2Bxml%3Bbase64%2CPHN2ZyB2aWV3Qm94PSIwIDAgMjQgMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI%2BPHBhdGggZD0iTTMuNDAzIDIuNDdhMi4wNiAyLjA2IDAgMCAwLTIuMDYgMi4wNnYxMi4zNjRhMi4wNiAyLjA2IDAgMCAwIDIuMDYgMi4wNmg3Ljk4M2wxLjc1OCAxLjY1NGEuMzEuMzEgMCAwIDAgLjM5OS4wMmwxLjI3LTEuMDI0Yy4zODguMTQ4Ljc2OC4yMjcgMS4xNjYuMjY2bC42ODcgMS40ODlhLjMwNS4zMDUgMCAwIDAgLjM2Mi4xNTdsMi4zMzQtLjcwNGEuMzA4LjMwOCAwIDAgMCAuMjE1LS4zMzVsLS4yNDUtMS42MmMuMTUxLS4xMjUuMjk2LS4yNTUuNDM0LS40MDIuMTM4LS4xNDcuMjYyLS4zMDcuMzg3LS40NzVsMS42Mi4xNTdhLjMxLjMxIDAgMCAwIC4zMjEtLjIzNWwuNTU0LTIuMzc0YS4zLjMgMCAwIDAtLjE4LS4zNTNsLTEuNTI1LS41OTVhNC40MTYgNC40MTYgMCAwIDAtLjM0Mi0xLjE0MWwuOTQyLTEuMzM0YS4zMS4zMSAwIDAgMC0uMDQ2LS4zOTdsLTEuNjEtMS41MDlWNC41MzFhMi4wNiAyLjA2IDAgMCAwLTIuMDYtMi4wNnptMCA0LjEyMWgxNC40MjN2NC4zODNjLS4zMS0uMS0uNjItLjE2OC0uOTQtLjIwNGwtLjY5LTEuNDgzYS4zMDEuMzAxIDAgMCAwLS4zNjQtLjE1NmwtMi4zMzQuNzA0YS4zMDEuMzAxIDAgMCAwLS4yMTUuMzM0bC4yNiAxLjYwNmMtLjE2LjEzNC0uMzEyLjI3LS40NS40MTZhNC43NDMgNC43NDMgMCAwIDAtLjM3NC40NmwtMS42MzEtLjE0MmEuMy4zIDAgMCAwLS4zMi4yMzVsLS41NTYgMi4zNzVhLjMwNi4zMDYgMCAwIDAgLjE4LjM1MmwxLjUyOS41OWMuMDQ2LjI4NS4xMjMuNTYuMjIxLjgzM0gzLjQwM3ptMTIuOTkxIDYuNTk2YTIuMTM2IDIuMTM2IDAgMCAxIDEuNDk3LjU3OCAyLjEzNiAyLjEzNiAwIDAgMSAuMDk3IDMuMDIgMi4xMzYgMi4xMzYgMCAwIDEtMy4wMTguMDk0IDIuMTM2IDIuMTM2IDAgMCAxLS4wOTctMy4wMTggMi4xMzYgMi4xMzYgMCAwIDEgMS41MjEtLjY3NHoiIHN0eWxlPSJmaWxsOiNlZjUzNTA7c3Ryb2tlLXdpZHRoOjEuMDMwMiIvPjwvc3ZnPg%3D%3D
[Makefile-url]: https://www.gnu.org/s/make/manual/make.html
[PostgreSQL]: https://img.shields.io/badge/PostgreSQL-4169E1?style=for-the-badge&logo=postgresql&logoColor=white
[PostgreSQL-url]: https://www.postgresql.org/
[PWA]: https://img.shields.io/badge/Progressive%20Web%20Apps-5A0FC8?style=for-the-badge&logo=pwa&logoColor=white
[PWA-url]: https://web.dev/i18n/pt/progressive-web-apps/
[Quasar.js]: https://img.shields.io/badge/quasar-1976d2?style=for-the-badge&logo=quasar&logoColor=white
[Quasar-url]: https://quasar.dev
[Vagrant]: https://img.shields.io/badge/Vagrant-1868F2?style=for-the-badge&logo=vagrant&logoColor=white
[Vagrant-url]: https://www.vagrantup.com/
[Vite]: https://img.shields.io/badge/Vite-646CFF?style=for-the-badge&logo=vite&logoColor=white
[Vite-url]: https://vitejs.dev/
[Vitest]: https://img.shields.io/badge/Vitest-6E9F18?style=for-the-badge&logo=vitest&logoColor=white
[Vitest-url]: https://vitest.dev/
[Vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
