import { defineConfig } from "histoire";
import { defaultColors } from "histoire";
import { HstVue } from "@histoire/plugin-vue";
import { HstQuasar } from "./src/histoire-plugin-quasar";

export default defineConfig({
  outDir: "../public",
  plugins: [HstVue(), HstQuasar()],
  setupFile: {
    browser: "histoire.setup.js",
  },
  storyIgnored: ["**/node_modules/**", "**/dist/**", "**/src-tauri/**"],
  theme: {
    title: "Djengu Framework Docs",
    logo: {
      square: "/src/assets/quasar-logo-full.svg",
      light: "/src/assets/djengu-logo.svg",
      dark: "/src/assets/djengu-logo-dark.svg",
    },
    colors: {
      primary: defaultColors.blue,
    },
    logoHref: "https://gitlab.com/fslawliet/djengu-framework",
    defaultColorScheme: "dark",
    hideColorSchemeSwitch: false,
    storeColorScheme: false,
  },
  routerMode: "history",
  tree: {
    groups: [
      {
        id: "top",
        title: "", // No toggle
      },
      {
        id: "pages",
        title: "Pages",
      },
      {
        id: "comp",
        title: "Components",
      },
      {
        id: "quasar",
        title: "Quasar",
      },
    ],
  },
  collectMaxThreads: 4,
  vite: {
    base: "/djengu-framework/",
  },
});
