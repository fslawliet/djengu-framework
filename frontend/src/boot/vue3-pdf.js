import { boot } from "quasar/wrappers";
import vue3ToPdf from "vue3-to-pdf";

export default boot(({ app }) => {
  app.use(vue3ToPdf);
});
