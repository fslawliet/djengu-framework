import { fileURLToPath } from "url";
import { join } from "path";

const ignorePlugins = ["quasar:index-html-transform"];

/**
 * Extract Quasar Vite Config
 * See: https://github.com/quasarframework/quasar-testing/blob/dev/packages/e2e-cypress/src/helpers/cct-dev-server/index.ts
 *
 */
async function quasarSharedConfig(quasarAppPackage) {
  const extensionRunner = await import(
    `${quasarAppPackage}/lib/app-extension/extensions-runner`
  );
  const getQuasarCtx = await import(
    `${quasarAppPackage}/lib/helpers/get-quasar-ctx`
  );
  const QuasarConfFile = await import(
    `${quasarAppPackage}/lib/quasar-config-file`
  );

  const ctx = getQuasarCtx({
    mode: "spa",
    target: void 0,
    debug: false,
    dev: true,
    prod: false,
  });

  // register app extensions
  await extensionRunner.registerExtensions(ctx);

  return {
    quasarAppPackage,
    QuasarConfFile,
    ctx,
  };
}

/**
 * Extract Quasar Vite Config
 * See: https://github.com/quasarframework/quasar-testing/blob/dev/packages/e2e-cypress/src/helpers/cct-dev-server/index.ts
 *
 */
async function quasarViteConfig(quasarAppPackage = "@quasar/app-vite") {
  const { QuasarConfFile, ctx } = await quasarSharedConfig(quasarAppPackage);

  const quasarConfFile = new QuasarConfFile({ ctx });

  const quasarConf = await quasarConfFile.read();
  if (quasarConf.error !== void 0) {
    console.log(quasarConf.error);
  }

  const generateConfig = await import(
    `${quasarAppPackage}/lib/modes/spa/spa-config`
  );

  // [1] -> https://github.com/cypress-io/cypress/issues/22505#issuecomment-1277855100
  // [1] Make sure to use the root for predictability
  quasarConf.publicPath = "/";

  const result = await generateConfig["vite"](quasarConf);

  // [1] Delete base so it can correctly be set by Cypress
  delete result.base;

  return result;
}

/**
 * Histoire Plugin for Quasar Apps
 *
 * See Nuxt plugin: https://github.com/histoire-dev/histoire/blob/main/packages/histoire-plugin-nuxt/src/index.ts
 * @returns
 */
export function HstQuasar() {
  return {
    name: "@histoire/plugin-quasar",

    async defaultConfig() {
      const viteConfig = await quasarViteConfig();

      const plugins = viteConfig.plugins.filter(
        (p) => !ignorePlugins.includes(p?.name)
      );
      return {
        vite: {
          //define: viteConfig.define,
          resolve: {
            alias: viteConfig.resolve.alias,
            extensions: viteConfig.resolve.extensions,
            dedupe: viteConfig.resolve.dedupe,
          },
          plugins,
          css: viteConfig.css,
          publicDir: viteConfig.publicDir,
          optimizeDeps: viteConfig.optimizeDeps,
          // @ts-expect-error Vue-specific config
          vue: viteConfig.vue,
        },
      };
    },
  };
}
