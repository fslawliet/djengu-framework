const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        name: "home",
        component: () => import("pages/IndexPage.vue"),
      },
      {
        path: "vermelho",
        name: "vermelho",
        component: () => import("pages/RedPage.vue"),
      },
      {
        path: "amarelo",
        name: "amarelo",
        component: () => import("pages/YellowPage.vue"),
      },
      {
        path: "verde",
        name: "verde",
        component: () => import("pages/GreenPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
