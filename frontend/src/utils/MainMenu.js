export const mainMenu = [
  {
    title: "Páginas",
    menus: [
      {
        title: "Páginas Vermelhas",
        target: "vermelho",
        caption: "Exemplo de Navegação",
        icon: "code",
      },
      {
        title: "Páginas Amarelas",
        target: "amarelo",
        caption: "Exemplo de Navegação",
        icon: "code",
      },
      {
        title: "Páginas Verdes",
        target: "verde",
        caption: "Exemplo de Navegação",
        icon: "code",
      },
    ],
  },
];
