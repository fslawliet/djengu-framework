import { installQuasarPlugin } from "@quasar/quasar-app-extension-testing-unit-vitest";
import { mount } from "@vue/test-utils";
import { beforeEach, describe, expect, it } from "vitest";
import ProfileCard from "../../../src/components/ProfileCard.vue";

installQuasarPlugin();

let wrapper;

beforeEach(() => {
  wrapper = mount(ProfileCard, {
    props: {
      id: "123456",
      avatar: "https://cdn.quasar.dev/img/boy-avatar.png",
      name: "AAE IdeaPro",
      title: "UI/UX Designer",
      shots: 69,
      followers: 2747,
      following: 678,
      lithning: 78,
      dark: false,
    },
  });
});

describe("Profile Card Component", () => {
  it("should have prop 'id'", () => {
    expect(typeof wrapper.vm.id).toBe("string");
    expect(wrapper.vm.id).toBe("123456");
  });
  it("should have prop 'avatar'", () => {
    expect(typeof wrapper.vm.avatar).toBe("string");
    expect(wrapper.vm.avatar).toBe("https://cdn.quasar.dev/img/boy-avatar.png");
  });
  it("should have prop 'name'", () => {
    expect(typeof wrapper.vm.name).toBe("string");
    expect(wrapper.vm.name).toBe("AAE IdeaPro");
  });
  it("should have prop 'title'", () => {
    expect(typeof wrapper.vm.title).toBe("string");
    expect(wrapper.vm.title).toBe("UI/UX Designer");
  });
  it("should have prop 'shots'", () => {
    expect(typeof wrapper.vm.shots).toBe("number");
    expect(wrapper.vm.shots).toBe(69);
  });
  it("should have prop 'followers'", () => {
    expect(typeof wrapper.vm.followers).toBe("number");
    expect(wrapper.vm.followers).toBe(2747);
  });
  it("should have prop 'following'", () => {
    expect(typeof wrapper.vm.following).toBe("number");
    expect(wrapper.vm.following).toBe(678);
  });
  it("should have prop 'lithning'", () => {
    expect(typeof wrapper.vm.lithning).toBe("number");
    expect(wrapper.vm.lithning).toBe(78);
  });
  it("should have prop 'dark'", () => {
    expect(typeof wrapper.vm.dark).toBe("boolean");
    expect(wrapper.vm.dark).toBe(false);
  });
  it("should have function 'edit' and emit event 'editProfile' on button click", async () => {
    expect(typeof wrapper.vm.edit).toBe("function");
    wrapper.find("#editBtn").trigger("click");
    await wrapper.vm.$nextTick();
    // assert event has been emitted
    expect(wrapper.emitted().editProfile).toBeTruthy();
    // assert event count
    expect(wrapper.emitted().editProfile.length).toBe(1);
    // assert event payload
    expect(typeof wrapper.emitted().editProfile[0][0]).toBe("string");
    expect(wrapper.emitted().editProfile[0][0]).toEqual("123456");
    expect(typeof wrapper.emitted().editProfile[0][1]).toBe("function");
  });
  it("should have function 'message' and emit event 'messageProfile' on button click", async () => {
    expect(typeof wrapper.vm.message).toBe("function");
    wrapper.find("#messageBtn").trigger("click");
    await wrapper.vm.$nextTick();
    // assert event has been emitted
    expect(wrapper.emitted().messageProfile).toBeTruthy();
    // assert event count
    expect(wrapper.emitted().messageProfile.length).toBe(1);
    // assert event payload
    expect(typeof wrapper.emitted().messageProfile[0][0]).toBe("string");
    expect(wrapper.emitted().messageProfile[0][0]).toEqual("123456");
    expect(typeof wrapper.emitted().messageProfile[0][1]).toBe("function");
  });
  it("should have function 'follow' and emit event 'followProfile' on button click", async () => {
    expect(typeof wrapper.vm.follow).toBe("function");
    wrapper.find("#followBtn").trigger("click");
    await wrapper.vm.$nextTick();
    // assert event has been emitted
    expect(wrapper.emitted().followProfile).toBeTruthy();
    // assert event count
    expect(wrapper.emitted().followProfile.length).toBe(1);
    // assert event payload
    expect(typeof wrapper.emitted().followProfile[0][0]).toBe("string");
    expect(wrapper.emitted().followProfile[0][0]).toEqual("123456");
    expect(typeof wrapper.emitted().followProfile[0][1]).toBe("function");
  });
});
