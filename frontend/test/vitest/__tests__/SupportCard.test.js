import { installQuasarPlugin } from "@quasar/quasar-app-extension-testing-unit-vitest";
import { mount } from "@vue/test-utils";
import { beforeEach, describe, expect, it } from "vitest";
import SupportCard from "../../../src/components/SupportCard.vue";

installQuasarPlugin();

let wrapper;

beforeEach(() => {
  wrapper = mount(SupportCard, {
    props: {
      id: "123456",
      name: "AAE IdeaPro",
      loading: false,
      dark: false,
    },
  });
});

describe("Support Card Component", () => {
  it("should have prop 'id'", () => {
    expect(typeof wrapper.vm.id).toBe("string");
    expect(wrapper.vm.id).toBe("123456");
  });
  it("should have prop 'name'", () => {
    expect(typeof wrapper.vm.name).toBe("string");
    expect(wrapper.vm.name).toBe("AAE IdeaPro");
  });
  it("should have prop 'loading'", () => {
    expect(typeof wrapper.vm.loading).toBe("boolean");
    expect(wrapper.vm.loading).toBe(false);
  });
  it("should have prop 'dark'", () => {
    expect(typeof wrapper.vm.dark).toBe("boolean");
    expect(wrapper.vm.dark).toBe(false);
  });
  it("should have function 'campaign' and emit event 'campaignStatus' on button click", async () => {
    expect(typeof wrapper.vm.campaign).toBe("function");
    wrapper.find("#campaignBtn").trigger("click");
    await wrapper.vm.$nextTick();
    // assert event has been emitted
    expect(wrapper.emitted().campaignStatus).toBeTruthy();
    // assert event count
    expect(wrapper.emitted().campaignStatus.length).toBe(1);
    // assert event payload
    expect(typeof wrapper.emitted().campaignStatus[0][0]).toBe("string");
    expect(wrapper.emitted().campaignStatus[0][0]).toEqual("123456");
    expect(typeof wrapper.emitted().campaignStatus[0][1]).toBe("function");
  });
  it("should have function 'support' and emit event 'supportAction' on button click", async () => {
    expect(typeof wrapper.vm.support).toBe("function");
    wrapper.find("#supportBtn").trigger("click");
    await wrapper.vm.$nextTick();
    // assert event has been emitted
    expect(wrapper.emitted().supportAction).toBeTruthy();
    // assert event count
    expect(wrapper.emitted().supportAction.length).toBe(1);
    // assert event payload
    expect(typeof wrapper.emitted().supportAction[0][0]).toBe("string");
    expect(wrapper.emitted().supportAction[0][0]).toEqual("123456");
    expect(typeof wrapper.emitted().supportAction[0][1]).toBe("number");
    expect(wrapper.emitted().supportAction[0][1]).toEqual(wrapper.vm.quantity);
    expect(typeof wrapper.emitted().supportAction[0][2]).toBe("function");
  });
});
